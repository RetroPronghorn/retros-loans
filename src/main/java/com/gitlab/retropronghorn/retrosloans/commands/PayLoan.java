package com.gitlab.retropronghorn.retrosloans.commands;

import java.text.NumberFormat;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Language;
import com.gitlab.retropronghorn.retrosloans.utils.Messenger;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class PayLoan {
    private RetrosLoans instance;

    public PayLoan(RetrosLoans instance) {
        this.instance = instance;
    }

    public void execute(CommandSender sender, Double amount) {
        boolean paid = instance.getBroker().makePayment((Player) sender, amount);
        if (paid) {
            Messenger.sendPlayerMessage(
                (Player) sender,
                Language.format(
                    Language.LOAN_PAID,
                    ChatColor.GREEN + NumberFormat.getInstance().format(amount)
                )
            );
        } else {
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(((Player) sender).getUniqueId());
            if (instance.getEconomy().getBalance(offlinePlayer) < amount) {
                Messenger.sendPlayerMessage(
                    (Player) sender,
                    Language.format(Language.CANNOT_AFFORD)
                );
            } else {
                Messenger.sendPlayerMessage(
                    (Player) sender,
                    Language.format(Language.PAYMENT_FAILED)
                );
            }
        }
    }
}
