package com.gitlab.retropronghorn.retrosloans.commands;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Language;
import com.gitlab.retropronghorn.retrosloans.constants.Permissions;
import com.gitlab.retropronghorn.retrosloans.utils.Messenger;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Executor implements CommandExecutor {
    private RetrosLoans instance;
    public TakeLoan takeLoan;
    public PayLoan payLoan;
    public Balance balance;
    public Cooldown cooldown;
    public List list;
    public Info info;
    public OpenGUI openGUI;

    public Executor(RetrosLoans instance) {
        this.instance = instance;
        takeLoan = new TakeLoan(instance);
        payLoan = new PayLoan(instance);
        balance = new Balance(instance);
        cooldown = new Cooldown(instance);
        list = new List(instance);
        info = new Info(instance);
        openGUI = new OpenGUI(instance);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (Permissions.hasPermission(Permissions.TAKE, sender)) {
            if (args.length == 2 && args[0].equalsIgnoreCase("take")) {
                takeLoan.execute(sender, args[1]);
                return true;
            } else if (args.length == 1 && args[0].equalsIgnoreCase("take")) {
                Messenger.sendPlayerMessage(
                    (Player) sender,
                    Language.format(Language.SPECIFY_LOAN)
                );
                return true;
            }
        }

        if (Permissions.hasPermission(Permissions.PAY, sender)) {
            if (args.length == 2 && args[0].equalsIgnoreCase("pay")) {
                try {
                    Double amount = Double.parseDouble(args[1]);
                    payLoan.execute(sender, amount);
                } catch(NumberFormatException exception) {
                    Messenger.sendPlayerMessage(
                        (Player) sender,
                        Language.format(Language.INVALID_NUMBER)
                    );
                }
                return true;
            } else if (args.length == 1 && args[0].equalsIgnoreCase("pay")) {
                Messenger.sendPlayerMessage(
                    (Player) sender,
                    Language.format(Language.INVALID_NUMBER)
                );
                return true;
            }
        }

        if (Permissions.hasPermission(Permissions.BALANCE, sender)) {
            if (args.length == 1 && args[0].equalsIgnoreCase("balance")) {
                balance.execute(sender);
                return true;
            }
        }

        if (Permissions.hasPermission(Permissions.COOLDOWN, sender)) {
            if (args.length == 1 && args[0].equalsIgnoreCase("cooldown")) {
                cooldown.execute(sender);
                return true;
            }
        }

        if (Permissions.hasPermission(Permissions.COOLDOWN, sender)) {
            if (args.length == 1 && args[0].equalsIgnoreCase("gui")) {
                openGUI.execute(sender);
                return true;
            }
        }

        if (args.length == 1 && args[0].equalsIgnoreCase("help")) {
            Help.execute(sender);
            return true;
        }

        if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
            list.execute(sender);
            return true;
        }

        // Default fallback command
        if (!instance.getBorrower((Player) sender).hasActiveLoan()) {
            list.execute(sender);
        } else {
            info.execute(sender);
        }

        return true;
    }

}