package com.gitlab.retropronghorn.retrosloans.commands;

import java.time.Instant;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Language;
import com.gitlab.retropronghorn.retrosloans.models.Broker;
import com.gitlab.retropronghorn.retrosloans.utils.Messenger;
import com.gitlab.retropronghorn.retrosloans.utils.Time;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class TakeLoan {
    private RetrosLoans instance;

    /**
     * Construct a new take loan handler
     * @param instance reference to plugin instance
     */
    public TakeLoan(RetrosLoans instance) {
        this.instance = instance;
    }

    /**
     * Takes out a new loan on a given command sender
     * @param sender the sender requesting a loan
     * @param loan the key of the loan they are requesting
     */
    public void execute(CommandSender sender, String loan) {
        boolean validLoan = instance.isValidLoan(loan);
        if (validLoan) {
            Broker broker = instance.getBroker();
            boolean loanIssued = broker.issueLoan(loan, (Player) sender);
            Long timeSinceLastLoan = Instant.now().toEpochMilli() - instance.getBorrower((Player) sender).getlastLoanIssuedAt();
            Long cooldownRemaining = instance.getConfig().getLong("cooldown") - timeSinceLastLoan;
            if (!loanIssued) {
                if (instance.getBorrower((Player) sender).hasActiveLoan()) {
                    Messenger.sendPlayerMessage(
                        (Player) sender,
                        Language.format(Language.OUTSTANDING_LOAN)
                    );
                } else if (timeSinceLastLoan < instance.getConfig().getLong("cooldown")) {
                    Messenger.sendPlayerMessage(
                        (Player) sender,
                        Language.format(
                            Language.COOLDOWN,
                            ChatColor.AQUA + Time.getDurationBreakdown(cooldownRemaining)
                        )
                    );
                } else {
                    Messenger.sendPlayerMessage(
                        (Player) sender,
                        Language.format(Language.NOT_ELIGABLE, loan)
                    );
                }
            }
        } else {
            // Invalid loan
            Messenger.sendPlayerMessage(
                (Player) sender,
                Language.format(Language.INVALID_LOAN, loan)
            );
        }
    }
}