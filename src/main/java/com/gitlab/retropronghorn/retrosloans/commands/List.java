package com.gitlab.retropronghorn.retrosloans.commands;

import java.text.NumberFormat;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Permissions;

import org.bukkit.command.CommandSender;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class List {
    private RetrosLoans instance;

    public List(RetrosLoans instance) {
        this.instance = instance;
    }

    public void execute(CommandSender sender) {
        sender.sendMessage("--------" +  ChatColor.GREEN + "" + ChatColor.BOLD + "Available Loans" + ChatColor.RESET + "--------");
        sender.sendMessage(" ");
        instance.getLoanKeys().forEach(loan -> {
            Double value = instance.getConfig().getDouble("loans."+loan+".amount");
            TextComponent message = new TextComponent(
                ChatColor.BOLD +
                loan +
                " - " +
                ChatColor.RESET +
                "$" +
                ChatColor.GREEN +
                NumberFormat.getInstance().format(value)
            );
            message.setClickEvent(
                new ClickEvent(
                    ClickEvent.Action.RUN_COMMAND,
                    "/loan take " + loan
                )
            );
            message.setHoverEvent(
                new HoverEvent(
                    HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("Click to take").create()
                )
            );
            sender.spigot().sendMessage(message);
        });

        sender.sendMessage(" ");
        sender.sendMessage(ChatColor.GRAY + "Click on a loan to request.");
    }
}