package com.gitlab.retropronghorn.retrosloans.listeners.vault;

import com.djrapitops.vaultevents.events.economy.PlayerDepositEvent;
import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Language;
import com.gitlab.retropronghorn.retrosloans.models.Borrower;
import com.gitlab.retropronghorn.retrosloans.utils.Messenger;
import com.gitlab.retropronghorn.retrosloans.utils.Numbers;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerDeposit implements Listener{
    private final RetrosLoans instance;

    /**
     * Create a new player deposit event
     * @param instance reference to plugin instance
     */
    public PlayerDeposit(RetrosLoans instance) {
        this.instance = instance;
    }

    @EventHandler
    public void playerDepositEvent(PlayerDepositEvent event) {
        OfflinePlayer player = event.getOfflinePlayer();
        Borrower borrower = instance.getBorrower(player.getUniqueId());
        if (borrower.hasActiveLoan() && event.getAmount() > 0) {
            // TODO: In a very rare edge case a player could be gifted
            // exactly their loan balance and avoid payments, let's find
            // a better way to handle this in the future.
            if (event.getAmount() != borrower.getLoan().getAmount()) {
                double incomeAfterDeductions = borrower.deductFromPayment(event.getAmount());
                Double toWithdraw = event.getAmount() - incomeAfterDeductions;
                instance.getEconomy().withdrawPlayer(
                    (OfflinePlayer) player,
                    event.getWorldName().toString(),
                    toWithdraw
                );
                Messenger.sendPlayerMessage(
                    Bukkit.getPlayer(player.getUniqueId()),
                    Language.format(
                        Language.PAYMENT_DEDUCTED,
                        Numbers.formatDouble(toWithdraw)
                    )
                );
            }
        }
    }
}