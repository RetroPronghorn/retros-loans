package com.gitlab.retropronghorn.retrosloans.gui;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.models.Borrower;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GUI {
    private RetrosLoans instance;
    protected ArrayList<ItemStack> items;
    ConfigurationSection icons;
    ConfigurationSection titles;

    public GUI(RetrosLoans instance) {
        this.instance = instance;
        this.icons = instance.getConfig()
            .getConfigurationSection("gui.icons");
        this.titles = instance.getConfig()
            .getConfigurationSection("gui.titles");
    }

    public ItemStack getBlankItem() {
        return new ItemStack(Material.LIGHT_GRAY_STAINED_GLASS_PANE);
    }

    public ItemStack getLoansItem() {
        ItemStack loans = new ItemStack(
            Material.getMaterial(
                icons.getString("loans")
            )
        );
        ItemMeta itemMeta = loans.getItemMeta();
        itemMeta.setDisplayName(
            titles.getString("loans")
        );
        loans.setItemMeta(itemMeta);
        return loans;
    }

    public ItemStack getPayItem() {
        ItemStack pay = new ItemStack(
            Material.getMaterial(
                icons.getString("pay")
            )
        );
        ItemMeta itemMeta = pay.getItemMeta();
        itemMeta.setDisplayName(
            titles.getString("pay")
        );
        pay.setItemMeta(itemMeta);
        return pay;
    }

    public ItemStack getBalanceItem() {
        ItemStack balance = new ItemStack(
            Material.getMaterial(
                icons.getString("balance")
            )
        );
        ItemMeta itemMeta = balance.getItemMeta();
        itemMeta.setDisplayName(
            titles.getString("balance")
        );
        balance.setItemMeta(itemMeta);
        return balance;
    }

    public ItemStack getCooldownItem() {
        ItemStack cooldown = new ItemStack(
            Material.getMaterial(
                icons.getString("cooldown")
            )
        );
        ItemMeta itemMeta = cooldown.getItemMeta();
        itemMeta.setDisplayName(
            titles.getString("cooldown")
        );
        cooldown.setItemMeta(itemMeta);
        return cooldown;
    }

    public ItemStack getHelpItem() {
        ItemStack help = new ItemStack(
            Material.getMaterial(
                icons.getString("help")
            )
        );
        ItemMeta itemMeta = help.getItemMeta();
        itemMeta.setDisplayName(
            titles.getString("help")
        );
        help.setItemMeta(itemMeta);
        return help;
    }

    public ItemStack getInfoItem() {
        ItemStack info = new ItemStack(
            Material.getMaterial(
                icons.getString("info")
            )
        );
        ItemMeta itemMeta = info.getItemMeta();
        itemMeta.setDisplayName(
            titles.getString("info")
        );
        info.setItemMeta(itemMeta);
        return info;
    }

    public ItemStack makeLoanItem(String name, Double amount) {
        ItemStack info = new ItemStack(
            Material.getMaterial(
                icons.getString("loan")
            )
        );
        ItemMeta itemMeta = info.getItemMeta();
        itemMeta.setDisplayName(StringUtils.capitalize(name));
        ConfigurationSection loan = instance.getConfig()
            .getConfigurationSection("loans." + name);
        Double rate = loan.getDouble("rate") * 100;
        List<String> lore = Arrays.asList(
            "$" + NumberFormat.getInstance().format(amount),
            "Interest " + rate.toString() + "%"
        );

        itemMeta.setLore(lore);
        info.setItemMeta(itemMeta);
        return info;
    }

    public Inventory getLoansInventory() {
        Inventory inventory = Bukkit.createInventory(
            null,
            9,
            titles.getString("all-loans")
        );
        instance.getLoanKeys()
            .forEach(l -> {
                inventory.addItem(
                    makeLoanItem(
                        l,
                        instance.getConfig().getDouble("loans." + l + ".amount")
                    )
                );
            });
        return inventory;
    }

    public Inventory getTailoredInventory(Player player) {
        Borrower borrower = instance.getBorrower(player);
        Inventory inventory = Bukkit.createInventory(
            null,
            9,
            titles.getString("main")
        );
        if (borrower.hasActiveLoan()) {
            inventory.addItem(getInfoItem());
            inventory.addItem(getBalanceItem());
            inventory.addItem(getHelpItem());
        } else {
            inventory.addItem(getLoansItem());
            inventory.addItem(getCooldownItem());
            inventory.addItem(getHelpItem());
        }
        return inventory;
    }
}