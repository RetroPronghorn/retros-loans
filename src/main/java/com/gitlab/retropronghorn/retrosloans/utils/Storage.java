package com.gitlab.retropronghorn.retrosloans.utils;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.models.Borrower;
import com.gitlab.retropronghorn.retrosloans.models.Loan;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitScheduler;

public class Storage {
    private RetrosLoans instance;
    private String fileName = "storage.yml";
    private File storageFile;
    protected final FileConfiguration storage = new YamlConfiguration();
    private BukkitScheduler scheduler = Bukkit.getServer().getScheduler();

    private int autosave;

    /**
     * Create a new storage helper
     * @param instance reference to the main plugin class
     */
    public Storage(RetrosLoans instance) {
        this.instance = instance;

        init();

        autosave = scheduler.scheduleSyncRepeatingTask(instance, new Runnable() {
            public void run() {
                Bukkit.getServer()
                    .getOnlinePlayers()
                    .forEach(player -> {
                        if (player.isOp()) {
                            Messenger.sendPlayerMessage(player, "Autosaving loans and borrowers.");
                        }
                    });
                saveBorrowers();
            }
        }, 0, instance.getConfig().getLong("storage.autosave-interval"));
    }

    /**
     * Create the storage file and load it into memory
     */
    public void init() {
        storageFile = new File(instance.getDataFolder(), fileName);
        if (!storageFile.exists()) {
            storageFile.getParentFile().mkdirs();
            instance.saveResource(fileName, false);
        }
        try {
            storage.load(storageFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public FileConfiguration getFile() {
        return storage;
    }

    /**
     * Cancel the autosave task and save the files to disk
     */
    public void shutdown() {
        scheduler.cancelTask(autosave);
        saveBorrowers();
        save();
    }

    /**
     * Set a new key value store and save
     *
     * @param node path to store at
     * @param value value to store
     */
    public void set(String node, String value) {
        storage.set(node, value);
    }

    /**
     * Get a node value
     *
     * @param node node to get from
     */
    public String get(String node) {
        return storage.getString(node);
    }

    /**
     * Get a list of keys from a specific node
     * @return a list of keys from the storage file
     */
    public Set<String> keys(String node) {
        return storage.getConfigurationSection(node).getKeys(false);
    }

    /**
     * Save the data storage file
     */
    public void save() {
        try {
            storage.save(storageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveBorrowers() {
        if (instance.getBorrowers().size() > 0) {
            instance.getBorrowers().keySet().forEach(uuid -> {
                String node = "borrowers." + uuid.toString();
                Borrower borrower = instance.getBorrower(uuid);
                storage.set(node + ".lifetime-payments", borrower.getLifetimePayments());
                storage.set(node + ".minimum", borrower.getMinimum());
                storage.set(node + ".trust", borrower.getTrust());
                storage.set(node + ".last-issued", borrower.getlastLoanIssuedAt());
                Loan loan = borrower.getLoan();
                if (loan!=null) {
                    storage.set(node + ".loan.balance", loan.getBalance());
                    storage.set(node + ".loan.amount", loan.getAmount());
                    storage.set(node + ".loan.rate", loan.getRate());
                    storage.set(node + ".loan.paid-off", loan.isPaidOff());
                }
                save();
            });
        }
    }

    public void loadBorrowers() {
        ConfigurationSection borrowers = storage.getConfigurationSection("borrowers");
        if (borrowers != null)
            borrowers.getKeys(false)
                .forEach(b -> {
                    Borrower borrower = new Borrower(
                        Bukkit.getPlayer(UUID.fromString(b)),
                        borrowers.getDouble(b + ".minimum"),
                        borrowers.getDouble(b + ".trust")
                    );
                    borrower.setLifetimePayments(
                        borrowers.getDouble(b + ".lifetime-payments")
                    );
                    if (borrowers.getDouble(b + ".loan.amount") != 0) {
                        borrower.setLoan(
                            borrowers.getDouble(b + ".loan.amount"),
                            borrowers.getDouble(b + ".loan.rate"),
                            borrowers.getLong(b + ".last-issued")
                        );
                        borrower.getLoan().setBalance(
                            borrowers.getDouble(b + ".loan.balance")
                        );
                        borrower.getLoan().setIsPaidOff(
                            borrowers.getBoolean(b + ".loan.paid-off")
                        );
                    }
                    instance.addBorrower(UUID.fromString(b), borrower);
                });
    }
}