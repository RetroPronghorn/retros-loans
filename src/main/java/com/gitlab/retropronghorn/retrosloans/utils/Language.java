package com.gitlab.retropronghorn.retrosloans.utils;

import java.io.File;
import java.io.IOException;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosutils.server.ULogger;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Language {
    private final RetrosLoans instance;
    private FileConfiguration lang = new YamlConfiguration();

    /**
     * Create a new language file in memory
     * @param instance reference to main plugin instance
     */
    public Language(RetrosLoans instance) {
        this.instance = instance;
        init();
    }

    /**
     * Get the language configuration file from memory
     * @return returns the language configuration
     */
    public FileConfiguration get() {
        return lang;
    }

    /**
     * Creates a default language file if none
     * is created yet then loads it into memory
     */
    private void init() {
        File langFile = new File(
            instance.getDataFolder(),
            "lang.yml"
        );
        // If lang file hasn't been created yet, load default
        if (!langFile.exists()) {
            langFile.getParentFile().mkdirs();
            instance.saveResource("lang.yml", false);
        }
        // Load to memory
        try {
            lang.load(langFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            ULogger.error(
                instance.getDescription().getName(),
                "Disabled due malformed yaml configuration."
            );
            instance.getServer()
                .getPluginManager()
                .disablePlugin(instance);
        }
    }
}