package com.gitlab.retropronghorn.retrosloans.events;

import com.gitlab.retropronghorn.retrosloans.models.Loan;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class LoanPaidOff extends Event {
    private static final HandlerList HANDLERS = new HandlerList();
    private Loan loan;

    /**
     * Construct a new loan paid off event
     * @param loan loan that was paid off
     */
    public LoanPaidOff(Loan loan) {
        this.loan = loan;
    }

    /**
     * Get list of handlers
     * @return returns a list of event handlers
     */
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    /**
     * Get list of handlers
     * @return returns a list of event handlers
     */
    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    /**
     * Get the loan the payment was made on
     * @return returns associated loan
     */
    public Loan getLoan() {
        return loan;
    }

    /**
     * Get the player associated with the event
     * @return returns the player associated with the event
     */
    public Player getPlayer() {
        return loan.getBorrower().getPlayer();
    }
}