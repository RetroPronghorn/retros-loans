package com.gitlab.retropronghorn.retrosloans;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.gitlab.retropronghorn.retrosloans.commands.Completer;
import com.gitlab.retropronghorn.retrosloans.commands.Executor;
import com.gitlab.retropronghorn.retrosloans.gui.GUI;
import com.gitlab.retropronghorn.retrosloans.gui.listeners.InventoryListener;
import com.gitlab.retropronghorn.retrosloans.hooks.Citizens;
import com.gitlab.retropronghorn.retrosloans.hooks.Vault;
import com.gitlab.retropronghorn.retrosloans.listeners.essentials.EssentialsIncome;
import com.gitlab.retropronghorn.retrosloans.listeners.vault.PlayerDeposit;
import com.gitlab.retropronghorn.retrosloans.models.Borrower;
import com.gitlab.retropronghorn.retrosloans.models.Broker;
import com.gitlab.retropronghorn.retrosloans.utils.Language;
import com.gitlab.retropronghorn.retrosloans.utils.Storage;
import com.gitlab.retropronghorn.retrosutils.server.ULogger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.milkbowl.vault.economy.Economy;

public class RetrosLoans extends JavaPlugin {
    private static RetrosLoans instance;
    private FileConfiguration language;
    private Economy economy;
    private Storage storage;
    private GUI gui = new GUI(this);

    private Broker broker = new Broker(this);

    private final HashMap<UUID, Borrower> borrowers = new HashMap<>();

    /**
     * Get plugin instance
     * @return returns plugin instance
     */
    public static RetrosLoans getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();
        // Plugin Startup
        instance = this;

        Language languageUtil = new Language(this);
        language = languageUtil.get();

        Vault vaultHook = new Vault(this);
        economy = vaultHook.get();

        if (economy != null) {
            storage = new Storage(this);
            // Commands
            getCommand("loan").setExecutor(new Executor(this));
            getCommand("loan").setTabCompleter(new Completer(this));
            // Events
            getServer().getPluginManager().registerEvents(new InventoryListener(this), this);

            if (Bukkit.getServer().getPluginManager().getPlugin("Essentials") != null) {
                hookEssentials();
            } else {
                getServer().getPluginManager().registerEvents(new PlayerDeposit(this), this);
            }

            if (Bukkit.getServer().getPluginManager().getPlugin("Citizens") != null) {
                hookCitizens();
            }
            // Load any borrowers from config
            storage.loadBorrowers();
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown
        System.out.println("Disabling--------------------" + economy);
        if (economy != null)
            storage.shutdown();
    }

    public void hookEssentials() {
        ULogger.info(
            instance.getDescription().getName(),
            "Essentials found, binding extra events."
        );
        getServer().getPluginManager().registerEvents(new EssentialsIncome(this), this);
    }

    public void hookCitizens() {
        ULogger.info(
            instance.getDescription().getName(),
            "Citizens found, binding extra events."
        );
        getServer().getPluginManager().registerEvents(new Citizens(), this);
    }

    // ----------------------------
    //          Getters
    // ----------------------------

    public GUI getGUI() {
        return gui;
    }

    /**
     * Get the language configuration file
     * @return returns the language configuration file
     */
    public FileConfiguration getLanguage() {
        return language;
    }

    /**
     * Get the economy api
     * @return returns reference to economy api
     */
    public Economy getEconomy() {
        return economy;
    }

    /**
     * Get a specific borrower by unique id
     * @param uuid unique id of the borrower
     * @return returns a borrower
     */
    public Borrower getBorrower(UUID uuid) {
        return borrowers.get(uuid);
    }

    /**
     * Get all borrowers registered
     * @return returns all registered borrowers
     */
    public HashMap<UUID, Borrower> getBorrowers() {
        return borrowers;
    }

    /**
     * Get the broker
     * @return the broker
     */
    public Broker getBroker() {
        return broker;
    }

    /**
     * Get all loan keys from config
     * @return returns all loan keys
     */
    public Set<String> getLoanKeys() {
        return getConfig()
            .getConfigurationSection("loans")
            .getKeys(false);
    }


    /**
     * Check that a loan exists in the config
     * @param key key of the loan to check
     * @return returns wether or not the loan exists
     */
    public boolean isValidLoan(String key) {
        return getConfig()
            .getConfigurationSection("loans")
            .getKeys(false)
            .contains(key);
    }

    /**
     * Get the minimum possible loan value
     * this will be the starting point for all borrowers
     * @return returns the minimum loan value
     */
    public ConfigurationSection getMinimumLoan() {
        ConfigurationSection loans = getConfig()
            .getConfigurationSection("loans");
        List<ConfigurationSection> sortedLoans = loans.getKeys(false).stream()
            .map(key -> loans.getConfigurationSection(key))
            .sorted((a, b) -> Double.compare(a.getDouble("amount"), b.getDouble("amount")))
            .collect(Collectors.toList());

        return sortedLoans.get(0);
    }

    /**
     * Get a borrower from a player
     * this will create a borrower if none exist
     * @param player player to get borrower info on
     * @return returns a Borrower for the given player
     */
    public Borrower getBorrower(Player player) {
        UUID uuid = player.getUniqueId();
        // If a borrower does not exist create one
        if (!borrowers.containsKey(uuid)) {
            borrowers.put(
                uuid,
                new Borrower(
                    player,
                    getMinimumLoan().getDouble("amount"),
                    getConfig().getDouble("trust")
                )
            );
        }
        return borrowers.get(uuid);
    }

    /**
     * Add a borrower from storage
     * @param uuid uuid of the borrower
     * @param borrower borrower instance
     */
    public void addBorrower(UUID uuid, Borrower borrower) {
        borrowers.put(uuid, borrower);
    }

}