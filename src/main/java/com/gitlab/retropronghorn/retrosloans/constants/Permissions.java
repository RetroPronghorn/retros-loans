package com.gitlab.retropronghorn.retrosloans.constants;

import org.bukkit.command.CommandSender;

public enum Permissions {
    COOLDOWN("retrosloans.loan.cooldown"),
    BALANCE("retrosloans.loan.balance"),
    PAY("retrosloans.loan.pay"),
    TAKE("retrosloans.loan.take"),
    GUI("retrosloans.loan.gui");

    private final String node;

    private Permissions(String permission) {
        node = permission;
    }

    public String get() {
        return node;
    }

    /**
     * Check if a user has permissions to a specific node
     * @param node node to check access on
     * @param sender command sender
     * @return returns wether or not the sender has access to the node
     */
    public static boolean hasPermission(Permissions node, CommandSender sender) {
        return sender.hasPermission(node.get()) || sender.isOp();
    }
}