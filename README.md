# Retro's Loans

Retro's Loans allows you to take loans which will be repaid every time a player earns money. A configurable percentage of each payment will be taken from economy events to repay the loan. Additionally you can configure the interest rate, meaning if you take a loan you'll owe X% more than the value of the loan.

Everything is configurable to your hearts content, from language, to the numbers, down to the display of the GUI.

## Economy Integrations

This plugin integrates with Vault so it should work with basic economy plugins. For some economy plugins hooks will need to be built but I am open to requests for your favorite economy plugin.

Right now we implement:
    - Essentials

These hooks bind to payment events on the player so that whenever the receive money they are forced to pay a bit back on their loan.

## Easy-pz

The plugin has well documented commands in `/help` and will dynamically display information to the user depending on their loan status so only the correct information is shown.

## Customize All The Things

All major (and most minor) parts of the plugin are configurable so you can tailor it to fit your server. Additionally making the plugin multi-lingual is made easy by the fact that all text displayed (except commands) are also configurable within the `lang.yml`

## General Info

### Loans

Loans can be aquired through the `/loan` or `/loan take <loan_name>` commands. You can add your own loans into the `config.yml`.

### Payments

Payments can be made manually by the player, in addtion payments will auto-deduct from income the player recieves until the loan is repaid.

### Help

Everything in the plugin is dynamically outlined in the `/loan help` command with clickable commands to make your player's life easier.

### Storage

Everything is kept in memory and auto-saved on a configurable interval to prevent data loss in the case of a crash.

### Notice

If you plan to remove this plugin from your server please note that there will be no way for the players to pay the loans they took so consider leaving the plugin on for a bit and disable the abiliy for new loans to be taken so that players can first pay off their loans. This will avoid harming your servers economy.

## Permissions & Commands

**/loan** - `no permissions needed`

Dynamicially displays information depending on loan status.

**/loan help** - `no permissions needed`

Get help using the plugin.

**/loan gui** - `retrosloans.loan.gui`

Open up a helpful GUI to take and manage loans.

**/loan take \<loan_name>** - `retrosloans.loan.take`

Take a new loan if you do not already have a pending loan.

**/loan balance** - `retrosloans.loan.balance`

Retruns the due balance on an active loan, if one exists.

**/loan pay \<amount>** - `retrosloans.loan.pay`

Allows a player to make a payment on their loan of a certain amount.

**/loan balance** - `retrosloans.loan.balance`

Returns the due balance on a loan.

**/loan cooldown** - `retrosloans.loan.cooldown`

Returns the cooldown the player must wait beore taking another loan.

## Show Me Screenshots

Okay :D Here ya go!

GUI
![img](https://i.imgur.com/XjsufoR.png)

Help
![img](https://i.imgur.com/dVBYZSs.png)

Auto pay
![img](https://i.imgur.com/KNNGoJb.png)

Dynamic /loan
![img](https://i.imgur.com/Phk0H6x.png)

Loans List
![img](https://i.imgur.com/zSVQB3x.png)

Fun Messaging & Sounds
![img](https://i.imgur.com/QVH3FQ2.png)